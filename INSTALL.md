
# Installation du programme badgeuse:

## Prérequis

* python3
* virtualenv

## Installation
```
# Copie du projet git
git clone https://gitlab.com/HATLab78/badgeuse.sqylab.git
# Se mettre dans le repertoire
cd badgeuse.sqylab
# Creation d'un environnement virtuel
vitualenv -p python3 venv
# Activation de l'environnement virtuel
source ./venv/bin/activate
# Installation des dépendances python
pip install -r requirements.txt
# Make sure the start script as execute permissions
chmod +x ./start.sh
# Lancement de l'application en mode developpement
./start.sh development
```

## Admin Password and Login

The admin password can be found in the file: `src/app_secrets_test.py`.

NEVER USE THIS FILE IN PRODUCTION.