import logging
from csv import DictReader, DictWriter
from datetime import datetime
from os import rename

from werkzeug.utils import secure_filename

from .utils.Profiler import profile


logging.basicConfig(level=logging.INFO)


class ObjetCSV(object):
    """Classe pour faciliter la gestion des objets d'un fichier csv."""
    def __init__(self, csvFile, constants):
        # super(ObjetCSV, self).__init__()
        self.csvFile = csvFile
        self = self.load_csv()
        self.c = constants

    # @profile
    def load_csv(self):
        with open(self.csvFile, mode='r') as csvReader:
            self.csvDict = self.read_csv(csvReader=csvReader)
            self.validate_csv()
            csvReader.close()

        return self

    # @profile
    def unload_csv(self):
        del self.csvDict

    # @profile
    def reload_csv(self):
        self.unload_csv()
        self = self.load_csv()

    # @profile
    def read_csv(self, csvReader):
        self.csvDict = DictReader(csvReader)
        self.fieldnames = self.csvDict.fieldnames
        self.csvDict = list(self.csvDict)

        return self.csvDict

    # @profile
    def write_csv(self):
        with open(self.csvFile, "w") as fichierCsv:
            writer = DictWriter(fichierCsv, self.fieldnames)
            writer.writeheader()
            for row in self.csvDict:
                writer.writerow(row)

        self.reload_csv()

    def ajouter_ligne_csv(self, ligneCsv, reload=True):
        ligneCsv = {k: v for k, v in ligneCsv.items() if k in self.fieldnames}
        with open(self.csvFile, "a") as entrees:
            entreesWriter = DictWriter(entrees, self.fieldnames)
            entreesWriter.writerow(ligneCsv)

        if reload:
            self.reload_csv()

        return ligneCsv

    def validate_csv(self):
        # print("test")
        # print(self.fieldnames)
        # print(self.csvDict)
        pass

    # @profile
    def rechercher(self, fieldValue, fieldName, exact=True, csvSource=None):
        if csvSource is None:
            csvSource = self.csvDict

        results = []
        if fieldName not in self.fieldnames:
            raise Exception("Field not in fieldnames")

        for line in csvSource:
            if isinstance(fieldValue, type("")):
                comparison = line[fieldName].lower() == fieldValue.lower()
            else:
                comparison = line[fieldName] == fieldValue
            if exact and comparison:
                results.append(line)
            elif not exact and fieldValue.lower() in line[fieldName].lower():
                results.append(line)
            # else:
            #     logging.info("Nothing to add... {}".format(line))

        return results

    # @profile
    def rechercher_dates(self, fieldValue, delta, fieldName=None, gt=True, csvSource=None):
        if fieldName is None:
            fieldName = self.c.DATE
        if csvSource is None:
            csvSource = self.csvDict

        results = []
        if fieldName not in self.fieldnames:
            raise Exception("Field not in fieldnames")

        for line in csvSource:
            comparator = self.convert_str_to_date(line[fieldName])
            compare = fieldValue - comparator
            # print("ObjetCSV - rechercher_dates: {}, {}, {}".format(compare, fieldValue, comparator))
            if gt and compare > delta:
                results.append(line)

            if compare == delta:
                results.append(line)

            if not gt and compare < delta:
                results.append(line)

        return results

    # @profile
    def rechercher_date(self, fieldValue, fieldName=None, gt=True, csvSource=None):
        print("ObjetCSV - rechercher_date: {}".format(fieldValue))
        if fieldName is None:
            fieldName = self.c.DATE
        if csvSource is None:
            csvSource = self.csvDict

        results = []
        if fieldName not in self.fieldnames:
            raise Exception("Field not in fieldnames")

        for line in csvSource:
            comparator = self.convert_str_to_date(line[fieldName])
            if (fieldValue.year - comparator.year) or (fieldValue.month - comparator.month):
                continue
            else:
                if (fieldValue.day - comparator.day) == 0:
                    results.append(line)

        return results

    # @profile
    def rechercher_heures(self, fieldValue, delta, fieldName=None, gt=True, csvSource=None):
        if fieldName is None:
            fieldName = self.c.HEURE
        if csvSource is None:
            csvSource = self.csvDict

        results = []
        if fieldName not in self.fieldnames:
            raise Exception("Field not in fieldnames")

        for line in csvSource:
            comparator = datetime.strptime(line[fieldName], self.c.HEURE_FORMAT)
            compare = datetime.strptime(fieldValue, self.c.HEURE_FORMAT) - comparator
            # print("ObjetCSV - rechercher_dates: {}, {}, {}".format(compare, fieldValue, comparator))
            if gt and compare > delta:
                results.append(line)

            if compare == delta:
                pass

            if not gt and compare < delta:
                results.append(line)

        return results

    # @profile
    def rechercher_nom(self, fieldValue, csvSource=None):
        return self.rechercher(fieldValue, fieldName=self.c.NOM, exact=False, csvSource=csvSource)

    # @profile
    def rechercher_prenom(self, fieldValue, csvSource=None):
        return self.rechercher(fieldValue, fieldName=self.c.PRENOM, exact=False, csvSource=csvSource)

    # @profile
    def rechercher_genre(self, fieldValue, csvSource=None):
        return self.rechercher(fieldValue, fieldName=self.c.GENRE, exact=False, csvSource=csvSource)

    # @profile
    def obtenir_date_actuelle(self):
        return datetime.today()

    # @profile
    def obtenir_heure_actuelle(self):
        return datetime.today().time().strftime(self.c.HEURE_FORMAT)

    # @profile
    def reecrire_fichier(self):
        archive = "{}-{}.csv".format(self.csvFile[:-4], str(self.obtenir_date_actuelle()))
        rename(self.csvFile, archive)
        self.write_csv()

    # @profile
    def secure_filename(self, filename):
        return secure_filename(filename)

    # @profile
    def convert_str_to_date(self, string):
        try:
            dateFin = datetime.strptime(string, self.c.DATE_FORMAT)
        except ValueError:
            dateFin = datetime.strptime(string, self.c.DATE_FORMAT_ALTERNATIF)

        return dateFin
