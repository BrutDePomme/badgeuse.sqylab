# coding: utf-8
from hashlib import md5
from os import rename
from os.path import isfile
from re import compile as re_compile
from threading import Lock, Thread, Timer
from time import sleep

from webdav3 import client as wc

from .Adherents import Adherents
from .Constants import Constants

adherentsLock = Lock()
majThread = Thread()
c = Constants()
POOL_TIME = 60 * 60 * 24
ADHERENTS_CSV_BACKUP = c.ADHERENTS_CSV.replace("adherents", "adherents-save")
# TODO don't download if already downloaded


def telecharger_nouveaux_adherents(nextcloudPassword):
    csvRegex = re_compile(r".*\.csv")

    options = {
        "webdav_hostname": "https://nextcloud.hatlab.fr",
        "webdav_login": "epouget",
        "webdav_password": nextcloudPassword,
        "webdav_root": "/remote.php/webdav/",
        "webdav_verbose": True,
    }

    client = wc.Client(options=options)
    remoteRootPath = "CSV_Badgeuse/Fichiers_Membres/"
    try:
        item = client.list(remoteRootPath)
        print(
            "mise_a_jour - telecharger_nouveaux_adherents - remote list: {}".format(
                item
            )
        )
        item = [i for i in item if csvRegex.match(i)]
        assert len(item) == 1, "too many csv files on remote: {}"
        membres = item[0]
        print(
            "mise_a_jour - telecharger_nouveaux_adherents - remote file: {}".format(
                membres
            )
        )
        item = client.info(remoteRootPath + membres)
        print(
            "mise_a_jour - telecharger_nouveaux_adherents - remote path: {}".format(
                item
            )
        )
        item = client.download(remoteRootPath + membres, c.ADHERENTS_CSV)
        print(
            "mise_a_jour - telecharger_nouveaux_adherents - download status: {}".format(
                item
            )
        )
        print(
            "mise_a_jour - telecharger_nouveaux_adherents - file exists: {}".format(
                isfile(c.ADHERENTS_CSV)
            )
        )
    except Exception as e:
        print(e)
        annuler_telecharger_nouveaux_adherents()
    # TODO fix, cannot work because file is skimmed of empty lines
    # nouveau = verifier_si_nouveau_fichier()
    # if not nouveau:
    #     print("mise_a_jour - telecharger_nouveaux_adherents - pas nouveau: {}".format(nouveau))
    #     return False
    # else:
    verification = verifier_nouveaux_adherents()
    if not verification:
        annuler_telecharger_nouveaux_adherents()

    return verification


def test_telecharger_nouveaux_adherents():
    with open(c.ADHERENTS_CSV, "w") as emptyFile:
        emptyFile.write("toto")
    return False


def verifier_nouveaux_adherents():
    adherents = Adherents(csvFile=c.ADHERENTS_CSV)
    ligneSupprime = adherents.supprimer_lignes_vides()
    print(
        "mise_a_jour - verifier_nouveaux_adherents - lignes supprimés: {}".format(
            ligneSupprime
        )
    )
    critique = adherents.renommer_entetes()
    print(
        "mise_a_jour - verifier_nouveaux_adherents - entêtes renomer: {}".format(
            critique
        )
    )
    print(
        "mise_a_jour - verifier_nouveaux_adherents - entêtes: {}".format(
            adherents.fieldnames
        )
    )
    warning = adherents.validate_csv()
    if warning is not True and "Assertion".lower() in str(warning).lower():
        print(
            "mise_a_jour - verifier_nouveaux_adherents - csv non validé: {}".format(
                warning
            )
        )
        return False
    else:
        print(
            "mise_a_jour - verifier_nouveaux_adherents - csv validé: {}".format(warning)
        )
        return True


def backup_adherents():
    print("mise_a_jour - backup_adherents - sauvegarde du précédent fichier adhérents.")
    rename(c.ADHERENTS_CSV, ADHERENTS_CSV_BACKUP)
    return True


def annuler_telecharger_nouveaux_adherents():
    print("mise_a_jour - annuler_telecharger_nouveaux_adherents - reprise de l'ancien fichier adhérent.")
    rename(ADHERENTS_CSV_BACKUP, c.ADHERENTS_CSV)
    return False


def verifier_si_nouveau_fichier():
    print("mise_a_jour - verifier_si_nouveau_fichier")
    nouveauHash = hash_md5(c.ADHERENTS_CSV)
    ancienHash = hash_md5(ADHERENTS_CSV_BACKUP)
    if nouveauHash == ancienHash:
        return False
    else:
        return True


def lancer_maj(nextcloudPassword):
    print("mise_a_jour - lancer_maj")
    secondesAvantMAJ = 20
    majThread = Timer(secondesAvantMAJ, maj_membres, [nextcloudPassword])
    majThread.daemon = True
    majThread.start()


def maj_membres(nextcloudPassword):
    global adherents
    global majThread
    resultat = backup_adherents()
    print("mise_a_jour - maj_membres - backup: {}".format(resultat))
    resultat = telecharger_nouveaux_adherents(nextcloudPassword)
    print("mise_a_jour - maj_membres - telechargement: {}".format(resultat))
    if resultat is not True:
        resultat = annuler_telecharger_nouveaux_adherents()
        print("mise_a_jour - maj_membres - annulation: {}".format(resultat))

    with adherentsLock:
        adherents = Adherents(csvFile=c.ADHERENTS_CSV)

    majThread = Timer(POOL_TIME, maj_membres)
    majThread.daemon = True
    majThread.start()


def interrupt_maj_membres():
    global majThread
    print("mise_a_jour - interrupt_maj_membres: stopping thread")
    majThread.cancel()


def maj_manuel():
    # resultat =
    telecharger_nouveaux_adherents()
    # print(resultat)
    delete = ""
    while delete != "y" and delete != "n":
        delete = input("Annuler? y / n")
        if delete == "n":
            break
        elif delete == "y":
            annuler_telecharger_nouveaux_adherents()
        else:
            continue


def hash_md5(fname):
    hash = md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash.update(chunk)
    return hash.hexdigest()
