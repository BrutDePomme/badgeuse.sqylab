
from .ObjetCSV import ObjetCSV


class Emails(ObjetCSV):
    """docstring for Emails."""
    def __init__(self, *args, **kwargs):
        super(Emails, self).__init__(*args, **kwargs)

    def ajouter_email(self, form):
        if form[self.c.NOM] and form[self.c.PRENOM] and form[self.c.EMAIL] and self.verifier_email_deja_enregistrer(form[self.c.EMAIL]):
            email = {self.c.NOM: form[self.c.NOM], self.c.PRENOM: form[self.c.PRENOM], self.c.EMAIL: form[self.c.EMAIL]}
            self.csvDict.append(email)
            self.write_csv()
            return email
        else:
            return False

    def verifier_email_deja_enregistrer(self, email):
        if self.rechercher_email(email):
            return False
        else:
            return True

    def rechercher_email(self, fieldValue, csvSource=None):
        self.rechercher(fieldValue, fieldName=self.c.EMAIL, csvSource=csvSource)

    def supprimer_email(self, email):
        nombreEmails = len(self.csvDict)
        for ligne in self.csvDict:
            if ligne and email.lower() in ligne[self.c.EMAIL].lower():
                break

        self.csvDict.remove(ligne)
        self.write_csv()

        if nombreEmails == len(self.csvDict):
            return False
        else:
            return True
