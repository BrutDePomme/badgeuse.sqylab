import logging

from .Adherents import Adherents
from .ObjetCSV import ObjetCSV


logging.basicConfig(level=logging.INFO)


class Formations(ObjetCSV):
    """Entrees permet de gérer le fichier adhérents et de le tester."""
    def __init__(self, *args, **kwargs):
        super(Formations, self).__init__(*args, **kwargs)

    def validate_csv(self):
        pass

    def validate_entetes_csv(self):
        assert self.c.EMAIL in self.fieldnames, "Entête manquante: {}".format(self.c.EMAIL)
        for machine in self.c.MACHINES:
            assert machine in self.fieldnames, "Entête manquante: {}".format(self.c.RFID)
        logging.info("Entêtes csv OK.")

    def rechercher_email(self, email):
        return self.rechercher(email, fieldName=self.c.EMAIL)

    def rechercher_formations(self, email):
        return self.rechercher_email(email)

    def supprimer_formation(self, email, machine):
        adherent = self.rechercher_adherent(email)
        for ligne in self.csvDict:
            if ligne[self.c.EMAIL] == email:
                ligne[machine] = False
                break
        else:
            return "Pas d'adhérent associé à cet ID"

        texte = "Vous avez supprimé la formation sur la {} de l'adhérent : {} {}".format(
            machine.upper(), adherent[self.c.PRENOM], adherent[self.c.NOM])

        self.write_csv()

        return texte

    def ajouter_formation(self, email, machine):
        adherent = self.rechercher_adherent(email)
        for ligne in self.csvDict:
            if ligne[self.c.EMAIL] == email:
                ligne[machine] = True
                texte = "Nouvelle formation sur la {} pour l'adhérent: {} {} enregistré.".format(
                    machine, adherent[self.c.PRENOM], adherent[self.c.NOM])
                break
        else:
            formationAdherent = {m: False for m in self.c.MACHINES}
            formationAdherent[self.c.EMAIL] = email
            formationAdherent[machine] = True
            self.csvDict.append(formationAdherent)
            texte = "Première formation, félicitation!\nFormation sur la {} pour l'adhérent: {} {} enregistré.".format(
                machine, adherent[self.c.PRENOM], adherent[self.c.NOM])

        self.write_csv()
        return texte

    def rechercher_adherent(self, email):
        with Adherents(csvFile=self.c.ADHERENTS_CSV) as adherents:
            adherent = adherents.rechercher_email(email)
            assert len(adherent) == 1, "Plus d'un adherent avec l'email: {}".format(email)

        return adherent[0]

    def a_recu_formation(self, adherent, machine):
        formations = self.rechercher_formations(adherent[self.c.EMAIL])
        return bool(formations[machine])
