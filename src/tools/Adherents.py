import logging
from datetime import date, datetime, timedelta

from unidecode import unidecode

from .ObjetCSV import ObjetCSV
from .utils.Validate import Validate


logging.basicConfig(level=logging.INFO)

# TODO Completer les tests


class Adherents(ObjetCSV):
    """Adherents permet de gérer le fichier adhérents et de le tester."""
    def __init__(self, *args, **kwargs):
        super(Adherents, self).__init__(*args, **kwargs)
        self.entetes = [self.c.GENRE, self.c.NOM, self.c.PRENOM, self.c.EMAIL, self.c.DATE_FIN]

    def validate_csv(self):
        try:
            self.validate_entetes_csv()
        except Exception as e:
            logging.critical("Fichier CSV avec des entêtes non valide: {}".format(e))
            return e
        for ligne in self.csvDict:
            try:
                dateValide = Validate(ligne[self.c.DATE_FIN])
                dateValide.tester_date()
                emailValide = Validate(ligne[self.c.EMAIL])
                emailValide.tester_email()
            except Exception as e:
                logging.warning("Exception, ligne: {} => {}".format(ligne, e))
                return e
        else:
            return True

    def validate_entetes_csv(self):
        for entete in self.entetes:
            assert entete in self.fieldnames, "Entête manquante: {}".format(entete)
        logging.info("Entêtes csv OK.")

    def renommer_entetes(self):
        fieldMap = {}
        for field in self.fieldnames:
            for entete in self.entetes:
                if unidecode(field).lower() == entete.lower():
                    fieldMap[field] = entete
                    break
            else:
                return "Erreur entête inconnue: {}, non trouvé dans: {}'".format(field, self.entetes)

        for ligne in self.csvDict:
            for field in self.fieldnames:
                value = ligne.pop(field)
                ligne[fieldMap[field]] = value

        self.fieldnames = [fieldMap[f] for f in self.fieldnames]
        self.write_csv()
        self.load_csv()
        print("Adherents - renommer_entetes: csv written")
        return True

    def supprimer_lignes_vides(self):
        newDict = []
        for ligne in self.csvDict:
            contenuLigne = "".join(ligne.values())
            if contenuLigne:
                newDict.append(ligne)

        if len(newDict) != len(self.csvDict):
            self.csvDict = newDict
            self.write_csv()
            return True
        else:
            return False

    def rechercher_email(self, fieldValue, csvSource=None):
        return self.rechercher(fieldValue, fieldName=self.c.EMAIL, csvSource=csvSource)

    def recherche_date_fin(self, fieldValue, csvSource=None):
        return self.rechercher(fieldValue, fieldName=self.c.DATE_FIN, csvSource=csvSource)

    def rechercher_adherent(self, nom=None, prenom=None):
        """Recherche un adherent et l'associe."""
        if nom and prenom:
            resultat = self.rechercher_nom(nom)
            resultat.extend(self.rechercher_prenom(nom))
        elif nom and not prenom:
            resultat = self.rechercher_nom(nom)
        else:
            resultat = self.rechercher_prenom(nom)

        if not resultat:
            logging.debug("Pas d'adhérent au nom de : {}".format(nom))
        else:
            logging.debug("Voici la liste des adhérents comportant: {} {}".format(nom, prenom))

        return resultat

    def rechercher_date_anniversaire(self, nom, prenom):
        for ligne in self.csvDict:
            if nom == ligne[self.c.NOM] and prenom == ligne[self.c.PRENOM]:
                dateAdhesion = ligne[self.c.DATE_FIN]

        return dateAdhesion

    def allowed_file(filename):
        logging.info("Fichiers authorisés")
        return "." in filename and filename.rsplit(".", 1)[1].lower() in self.c.ALLOWED_EXTENSIONS

    def mise_a_jour_adherents(self, fichier, cheminFichier):
        logging.info("Mise à jour adhérent")
        if fichier and self.allowed_file(fichier.filename) and self.validate_fichier_adherent(cheminFichier):
            nomDuFichier = self.secure_filename(fichier.filename)
            fichier.save(cheminFichier)
            self.reecrire_fichier()
            self.csvDict = self.read_csv(csvFile=cheminFichier)
            return nomDuFichier
        else:
            return self.validate_fichier_adherent()

    def adherent_a_jour(self, adherent):
        print("Adherent date de fin de cotisation: {}".format(adherent[self.c.DATE_FIN]))
        dateFin = self.convert_str_to_date(adherent[self.c.DATE_FIN])

        if (self.obtenir_date_actuelle() - dateFin) > timedelta(0):
            return True
        else:
            return False
