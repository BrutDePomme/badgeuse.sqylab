from flask import Blueprint
from flask import current_app as app
from flask import flash, redirect, render_template, request, session, url_for

from .config import CONSTANTS as c
from .tools.logger import log_request


bp = Blueprint('accueil', __name__, url_prefix="/accueil")


@bp.route(c.APP_PATHS[c.APP_ROOT], methods=[c.WEB_GET])
def get_accueil():
    log_request(c.APP_ACCUEIL, request)
    kwargs = {c.WEB_ACTIVE: c.APP_ACCUEIL}
    kwargs.update(c.APP_PATHS)
    entreesDuJour = app.manager.rechercher_entrees_du_jour()
    kwargs[c.WEB_ENTREES] = app.manager.colorer_entrees_avec_fin_cotisation(entreesDuJour)
    return render_template('{}.html'.format(c.APP_ACCUEIL), **kwargs)


@bp.route(c.APP_PATHS[c.APP_RECHERCHE], methods=[c.WEB_POST])  # @bp.route(c.APP_PATHS[c.APP_ACCUEIL]
def rechercher_adherent():
    """Affiche les entrées du jour sur la page d'accueil."""
    log_request(c.APP_PATHS[c.APP_RECHERCHE], request)
    kwargs = {c.WEB_ACTIVE: c.APP_ACCUEIL}
    kwargs.update(c.APP_PATHS)
    kwargs.update(request.form)
    session[c.WEB_ADHERENTS] = app.manager.rechercher_adherent(request.form[c.NOM])
    for item in session[c.WEB_ADHERENTS]:
        item[c.URI] = item[c.URI].format(URI="accueil/" + c.APP_SIMULER)
    kwargs[c.WEB_ADHERENT_INDEX] = list(range(len(session[c.WEB_ADHERENTS])))
    kwargs[c.WEB_CONTENU] = session[c.WEB_ADHERENTS]
    entreesDuJour = app.manager.rechercher_entrees_du_jour()
    kwargs[c.WEB_ENTREES] = app.manager.colorer_entrees_avec_fin_cotisation(entreesDuJour)
    return render_template('{}.html'.format(c.APP_ACCUEIL), **kwargs)


@bp.route(c.APP_PATHS[c.APP_VISITEUR], methods=[c.WEB_POST])  # @bp.route(c.APP_PATHS[c.APP_ACCUEIL]
def simuler_visiteur():
    log_request(c.APP_PATHS[c.APP_VISITEUR], request)
    kwargs = {c.WEB_ACTIVE: c.WEB_VISITEUR}
    kwargs.update(c.APP_PATHS)
    session[c.WEB_ADHERENT] = {k: v for k, v in request.form.items()}
    session[c.WEB_ADHERENT][c.GENRE] = "m."
    print("accueil - simuler_visiteur - SESSION: {}".format(session[c.WEB_ADHERENT]))
    kwargs.update(session[c.WEB_ADHERENT])

    resultat = app.manager.ajouter_visiteur(session[c.WEB_ADHERENT])
    message = flash_message(resultat)
    flash(*message)

    return redirect(url_for("accueil.get_accueil"))


@bp.route(c.APP_PATHS[c.APP_SIMULER], methods=[c.WEB_GET])
def simuler():
    log_request(c.APP_SIMULER, request)
    session[c.WEB_ADHERENT] = session[c.WEB_ADHERENTS][int(request.args[c.WEB_ADHERENT_INDEX])]
    print("Simuler adherent: {}".format(session[c.WEB_ADHERENT]))
    resultat = app.manager.ajouter_entree(session[c.WEB_ADHERENT])

    message = flash_message(resultat)
    flash(*message)

    return redirect(url_for("accueil.get_accueil"))


# TODO add the new adherent signup page
@bp.route(c.APP_PATHS[c.APP_ADHESION])
def retourner_adhesion():
    log_request(c.APP_ADHESION, request)
    # /adhesions/adhesions.php
    return render_template("501.html", active=c.APP_ADHESION, **c.APP_PATHS)


def flash_message(resultat):
    if not resultat[c.DEJA_SCANNE] and resultat[c.JOURS_RESTANTS] is not False:
        message = "Bonjour, {}! Bons projets!".format(session[c.WEB_ADHERENT][c.PRENOM])
        categorie = c.CAT_OK
    elif not resultat[c.DEJA_SCANNE]:
        message = ""
        categorie = c.CAT_OK
    else:
        if "m." not in session[c.WEB_ADHERENT][c.GENRE].lower():
            feminin = "e"
        else:
            feminin = ""
        message = "Bien tenté {} mais tu t'es déjà inscrit{}!".format(session[c.WEB_ADHERENT][c.PRENOM], feminin)
        categorie = c.CAT_ATTENTION

    if resultat[c.JOURS_RESTANTS] is False and not message:
        message += "\nBienvenu {}, bonne visite!".format(session[c.WEB_ADHERENT][c.PRENOM])
        return message, c.CAT_OK
    if resultat[c.JOURS_RESTANTS] is False:
        return message, c.CAT_ATTENTION
    elif resultat[c.JOURS_RESTANTS] >= c.JOURS_RAPPEL:
        return message, categorie
    elif resultat[c.JOURS_RESTANTS] <= c.JOURS_RAPPEL and resultat[c.JOURS_RESTANTS] > 0:
        message += "\nAttention plus que {} jours avant la fin de ta cotisation.".format(resultat[c.JOURS_RESTANTS])
        return message, c.CAT_ATTENTION
    else:
        message += "\nAttention date de cotisation dépassée!"
        return message, c.CAT_ERREUR
