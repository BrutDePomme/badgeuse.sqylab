import os
from re import compile as re_compile

from flask import Flask, g, session
from .tools.Manager import Manager
from .tools.Adherents import Adherents
from .tools.Entrees import Entrees
from .tools.Badges import Badges
from .tools.Emails import Emails
from .tools.Formations import Formations
# from .tools.Bugs import Bugs
# from .tools.Evenements import Evenements
from pprint import PrettyPrinter
import atexit

from .tools.utils.Profiler import funcMap
from .config import CONSTANTS as c


pp = PrettyPrinter(indent=4)

global funcMap


def teardown_print(something):
    pp.pprint({k: {"executions": len(v), "tottime": sum(v)/len(v)} for k, v in funcMap.items()})
    return True


PROFILE_FLASK = False


def profile_flask(app):
    from flask_profiler import Profiler
    profiler = Profiler()

    app.config["DEBUG"] = True

    # You need to declare necessary configuration to initialize
    # flask-profiler as follows:
    app.config["flask_profiler"] = {
        "enabled": app.config["DEBUG"],
        "storage": {
            "engine": "sqlite"
        },
        "basicAuth": {
            "enabled": True,
            "username": "admin",
            "password": "admin"
        },
        "ignore": [
            "^/static/.*"
        ]
    }

    profiler = Profiler()  # You can have this in another module
    profiler.init_app(app)
    return app


DATE_REGEX = re_compile(r"(\d{4})(-|/)(\d{2})(-|/)(\d{2})")


def filtre_date(date):
    print(date)
    resultat = DATE_REGEX.match(date)
    if resultat:
        print(resultat.groups())
        return "{}-{}-{}".format(resultat.groups()[4], resultat.groups()[2], resultat.groups()[0])
    else:
        return date


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object("src.config")

    app.config.from_pyfile('config.py', silent=True)
    if PROFILE_FLASK is True:
        app = profile_flask(app)
        # app.config["PROFILE"] = True

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # # a simple page that says hello
    # @app.route('/hello')
    # def hello():
    #     return 'Hello, World!'
    app.add_template_filter(filtre_date)

    from . import db
    db.init_app(app)

    adherents = Adherents(csvFile=c.ADHERENTS_CSV, constants=c)
    entrees = Entrees(csvFile=c.ENTREES_CSV, constants=c)
    badges = Badges(csvFile=c.BADGE_CSV, constants=c)
    emails = Emails(csvFile=c.VISITEURS_CSV, constants=c)
    formations = Formations(csvFile=c.FORMATIONS_CSV, constants=c)
    app.manager = Manager(adherents=adherents, entrees=entrees, badges=badges, emails=emails, formations=formations, constants=c)

    # Setup stats
    def flask_update_stats():
        g.visiteurCeJour, g.visiteurCetteSemaine, g.visiteurCeMois = entrees.stats
        g.visiteurUniqueCeJour, g.visiteurUniqueCetteSemaine, g.visiteurUniqueCeMois = entrees.unique_stats

    # # Ensure even the login form receives the APP_PATHS
    # @app.context_processor
    # def security_context_processor():
    #     return c.APP_PATHS

    app.before_request(flask_update_stats)
    # app.before_request(security_context_processor)

    from . import accueil
    app.register_blueprint(accueil.bp)
    app.add_url_rule('/', endpoint='accueil.get_accueil')

    from . import historique
    app.register_blueprint(historique.bp)

    from . import admin
    app.register_blueprint(admin.bp)

    from . import rfid
    app.register_blueprint(rfid.bp)
    # from . import evenement
    # app.register_blueprint(evenement.bp)
    app.teardown_appcontext(teardown_print)
    if app.config['ENV'] != "development":
        from flask_socketio import SocketIO
        socketio = SocketIO(app)
        app.socketio = socketio
        socketio.init_app(app)


    if app.config["WEBDAV_UPLOAD"] is True:
        # Lancement du thread de mise à jour
        from .tools.mise_a_jour import interrupt_maj_membres, lancer_maj
        lancer_maj(app.config["NEXTCLOUD_PASSWORD"])
        # Fermeture du thread si (SIGTERM)
        atexit.register(interrupt_maj_membres)

    # @app.socketio.on('logout')
    # def logout(item):
    #     print("__init__.py -- logout: {}".format(item))
    #     session.clear()

    return app


if __name__ == '__main__':
    app = create_app()
