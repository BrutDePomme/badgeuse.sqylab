from json import dumps

from flask import Blueprint, Response
from flask import current_app as app
from flask import flash, redirect, render_template, request, session, url_for

from .accueil import flash_message
from .tools.logger import log_request
from .config import CONSTANTS as c


bp = Blueprint('rfid', __name__, url_prefix="/rfid")


def valideRFID(adherent, rfid):
    if not adherent:
        return (False, {"response": "Not Acceptable: RFID non associé",
                        "status": 406, "mimetype": "application/text"})
    elif len(adherent) > 1:
        return (False, {"response": "Conflict: Plusieurs adhérents pour ce numéro RFID",
                        "status": 409, "mimetype": "application/text"})
    else:
        assert len(adherent) == 1
        return (True, adherent[0])


@bp.route("/", methods=[c.WEB_PUT])
def scanne_badgeuse():
    log_request(c.APP_ADHESION, request)
    rfid = request.form[c.RFID]
    adherent = app.manager.rechercher_adherent_par_rfid(rfid)

    validation = valideRFID(adherent, rfid)

    if validation[0] is True:
        session[c.WEB_ADHERENT] = validation[1]
        resultat = app.manager.ajouter_entree(session[c.WEB_ADHERENT])

        message = flash_message(resultat)
        flash(*message)
        template = render_template("includes/flash_messages.html")
        app.socketio.emit("rfid",  {"template": template}, namespace="")

        return dumps(session[c.WEB_ADHERENT])
    else:
        flash(validation[1]["response"], c.CAT_ATTENTION)
        template = render_template("includes/flash_messages.html")
        app.socketio.emit("rfid",  {"template": template}, namespace="")
        app.manager.dernierRfid = rfid
        return Response(**validation[1])


@bp.route("/<machine>", methods=[c.WEB_PUT])
def scanne_machine(machine):
    log_request(c.APP_ADHESION, request)
    rfid = request.form[c.RFID]
    adherent = app.manager.rechercher_adherent_par_rfid(rfid)
    if machine not in c.MACHINES:
        return Response("Not Acceptable: La machine n'est pas prise en charge", status=406, mimetype='application/text')

    validation = valideRFID(adherent, rfid)

    if validation[0] is True and app.manager.a_recu_formation(session[c.WEB_ADHERENT], machine):
        app.manager.ajouter_usage_machine(session[c.WEB_ADHERENT])
        return Response(session[c.WEB_ADHERENT], status=200, mimetype='application/json')
    elif validation[0] is True:
        return Response("Unauthorized: Adhérent non formé", status=401, mimetype='application/text')
    else:
        app.manager.dernierRfid = rfid
        return Response(**validation[1])
